'use strict';
myApp.controller('homeCtrl',function ($scope,$rootScope,apiService,ENV){

	var routeColorScale = d3.scale.category10();
		$scope.loadFlag = true;
		$scope.chartId = "chart";
		$scope.control = {};
		$scope.selectedroute = 'N';
		$scope.agencyList = [];
		$scope.selectedroutes = [];
		$scope.routeData = {};
		$scope.locationData = {};

		$scope.config = {
			showStops:true,
			showVehicles:true
		};


	var agencyMap = {},
		defaultAgency = 'sf-muni',
		routeObjects = [];

	/**********************API call to Fetch routes for a given Agency******************************/
	apiService.getMapdata(ENV.agency_list_url,'agencyList').then(function(data,error){
	 				$scope.agencyList = data;
	 				data.forEach(function(d){
	 					agencyMap[d.tag] = d.title;
	 				});


	 				/**********************API call to Fetch Base Map Geo JSON(Biginning of all rendering)**********/

					apiService.getMapdata(ENV.BASE_map_URL1,'baseMap').then(function(data,error){
					 				$scope.baseMapdata = data;
				                    $scope.add($scope.selectedroute);
				                    $scope.loadFlag = false;
				    });


					apiService.getMapdata(ENV.BASE_map_URL2,'baseMap').then(function(data,error){
					 				$scope.baseMapdata = data;
				                   $scope.add($scope.selectedroute);
				                   $scope.loadFlag = false;
				    });

    });



	/*****************Method to add new selected Routes from UI*************************************/
    $scope.add = function(_){

    	
    	var exists = false;
    		routeObjects.forEach(function(d){
    			console.log(d)
    			if(d.route == _)
    				exists = true;
    		});

    		if(!exists){
    			$scope.selectedroutes.push({route:agencyMap[_],color:routeColorScale(_)});
    			addRoute(_);
    		}
    }

    /******************Method to remove route and its instances**************************************/
    $scope.remove = function(_){
    	console.log(_);
    		$scope.selectedroutes.splice(_,1);
    	var removedroute = routeObjects.splice(_,1);
    		console.log(removedroute)
    		removedroute[0].removeRoute();

    }

    /*****************Map Zoom, exposed through Directive******************************/
	$scope.zoom = function(_){
		if(typeof $scope.control.zoom == 'function')
			$scope.control.zoom(_);
	}

	/*****************Keep Pulling vehicles Location once in every 15 seconds***********/
	setInterval(function(){
			routeObjects.forEach(function(d){
                    	d.updateLocation();
                    })
	},15000);

	/******************Common Method to create New route instance  **********************/
	function addRoute(d){
		var routeInstance = new routeObj(d,defaultAgency);
                    	routeInstance.fetchData();
                    	routeObjects.push(routeInstance)
	}



	/*********************route Constructor Object***************************************/
	function routeObj(route,agency){
		this.vehiclesStartDate = new Date();
		this.route = route;
		this.agency = agency;
		this.status = true;
		this.color = routeColorScale(this.route)
	}
	routeObj.prototype.fetchData = function(){

		var url = ENV.BASE_MAP_GPS_URL+'routeConfig&a='+this.agency+'&r='+this.route;
		var self = this;
		apiService.getMapdata(url,'routedata').then(function(data,error){
			/*****triggers updateRoute method inside Directive******/
			data.values.stops.color = routeColorScale(self.route);
			data.values.paths.color = routeColorScale(self.route);

			$scope.routeData[data.route] = data.values;
			/********Pull Bus Locations*******/
			self.updateLocation();
		});

	};
	routeObj.prototype.removeRoute = function(){
				console.log($scope.routeData);
		delete $scope.routeData[this.route];
		delete $scope.locationData[this.route];
				console.log($scope.routeData);
			this.status = false;
	}
	routeObj.prototype.updateLocation = function(){
		var endDate = new Date();
		var self = this;
		var url = ENV.BASE_MAP_GPS_URL+'vehicleLocations&a='+this.agency+'&r='+this.route+'&t='+(Date.parse(endDate)-Date.parse(this.vehiclesStartDate));
			
		apiService.getMapdata(url,'locationData').then(function(data,error){
			if(self.status){
				data.values.forEach(function(d){
					d.color = routeColorScale(self.route)
				})
				$scope.locationData[self.route] = data.values;
			}
		});

	};
	


});