/***********************************************
*       Developer: Narayana Swamy
*		email: narayanaswamy14@gmail.com
************************************************/

d3 = d3 || {};

(function(_){
	"use strict"	
	d3.SFMap = function(){

		var state = {},
			containerId,
			g_,
			width,
			height,
			container,
			locationData,
			capturedLatLon,
			showVehicles,
			showStops,
			v_width = 30,
			v_height = 15,
			zoom;

		var projection = d3.geo.albers(),
			path = d3.geo.path(),
			defaultzoom = 400000,
			bGroundDrag = d3.behavior.drag(),
			vehicleLocationMap = {};

		var resizeScale = d3.scale.linear()
							.domain([682,1157])
							.range([280000,400000]);

		var chart = function(selection){

			selection.each(function(d){
				/****** fetch Width/Height from the container dynamically *****************/
				width = parseInt(d3.select('#'+containerId).style('width'));
				height = parseInt(d3.select('#'+containerId).style('height'));

				/******** get Projection scale based on window width dimension *************/
				defaultzoom = resizeScale(width);

				container = d3.select(this);

				container.attr('width',width).attr('height',height);

				if(!g_){
					/********* create base groups once **********************************/
					g_ = container.append('g').attr('class','parent_g');

						g_.append('g').attr('class','baseMap')
						g_.append('g').attr('class','bGround').append('rect');
						g_.append('g').attr('class','routes');
						g_.append('g').attr('class','stops');
						g_.append('g').attr('class','location');

				}

				/********* set the canvas for Click functionality **********************/
				container.select('.bGround').select('rect')
						.attr('x',0)
						.attr('y',0)
						.attr('width',width)
						.attr('height',height)
						.style('fill','transparent')
						.on('click',function(){
								capturedLatLon = projection.invert(d3.mouse(this));
						});

				/********* set the canvas for Drag functionality ************************/
				g_.call(bGroundDrag);

				/********* prepare projection scale based on projection value ***************/
				projection.scale(defaultzoom)
							.translate([width/2, height/2 ]);

				path.projection(projection);

			});

			/********* on window resize event handler **************************************/
			d3.select(window).on('resize.'+containerId, chart.resize);

		}

		/********* register on Drag event handler *****************************/
		bGroundDrag.on('drag',function(e){

			var newTransform = d3.transform(g_.attr('transform')).translate;

				newTransform[0] = newTransform[0]+d3.event.dx;
				newTransform[1] = newTransform[1]+d3.event.dy;

				d3.select(this).select('.bGround')
								.select('rect')
								.attr('x',newTransform[0]*-1)
								.attr('y',newTransform[1]*-1);

				g_.attr('transform','translate('+newTransform[0]+','+newTransform[1]+')');

		});

		/********* Zoom functionality **************************************/
		chart.zoom = function(_){
			zoom = true;
			defaultzoom = defaultzoom + _;
			projection.scale(defaultzoom);
			path.projection(projection);
			chart.renderBaseMap(state.mapData);
		};

		/********* show/Hide vehicles **************************************/
		chart.showVehicles = function(_){
			showVehicles = _;
			if(!_)
				container.selectAll('.routeVehicles').remove();
			else
				if(state.locationData)					
					chart.locationData(state.locationData,true)
		};

		/********* show/Hide stops **************************************/
		chart.showStops = function(_){
			showStops = _;
			if(!_)
				container.selectAll('.stop').remove();
			else
				if(state.stopsData)
					chart.stopsData(state.stopsData);
		};	

		/********* on window resize event handler **************************************/
		chart.resize = function(){
			capturedLatLon = undefined;
			zoom = true;
			d3.select('#'+containerId).select('svg').call(chart);
			chart.renderBaseMap(state.mapData);
		};

		/********* getter/setter method for containerId private variable ***************/
		chart.containerId = function(_){
			if(!arguments.length) return containerId
			containerId = _;
			state.containerId = _;
			return chart;
		};

		/********* render baseMap , triggered from Directive ***************/
		chart.renderBaseMap = function(_){

				state.mapData = _;

			var lon = d3.min(state.mapData.features,function(d){
							return d3.min(d.geometry.coordinates,function(e){
								return e[0];
							})
						});
			var lonMax = d3.max(state.mapData.features,function(d){
							return d3.max(d.geometry.coordinates,function(e){
								return e[0];
							})
						});
			var lat = d3.min(state.mapData.features,function(d){
							return d3.min(d.geometry.coordinates,function(e){
								return e[1];
							})
						});
			var latMax = d3.max(state.mapData.features,function(d){
							return d3.max(d.geometry.coordinates,function(e){
								return e[1];
							})
						});

			var co = capturedLatLon?projection(capturedLatLon):projection([lon,lat]);
			var coMax = projection([lonMax,latMax]);

			var londiff = coMax[0]-co[0];
			var latdiff = coMax[1]-co[1];

				if(capturedLatLon){
					/************** Zoom at perticular area ***********************************************/

					container.select('.bGround').select('rect')
												.attr('x',((co[0])-width/2)).attr('y',((co[1])-height/2));
					
					var baseMap = g_.attr('transform','translate('+((co[0]*-1)+width/2)+','+((co[1]*-1)+height/2)+')')
									.select('.baseMap')
								  	.selectAll("path")
								    .data(state.mapData.features);

				}else{

					container.select('.bGround').select('rect')
												.attr('x',(((co[0]+londiff/4))-width/6)).attr('y',(((co[1]))-height/1.25));

					var baseMap = g_.attr('transform','translate('+(((co[0]+londiff/4)*-1)+width/6)+','+(((co[1])*-1)+height/1.25)+')')
								.select('.baseMap')
							  	.selectAll("path")
							    .data(state.mapData.features);
				}

				baseMap
					.enter()
					.append("path");

				baseMap.exit().remove();

				baseMap
				    .attr("d", path)
				    .style('stroke','grey')
					.style('stroke-width','0.1px')
					.style('fill','#BBA543');


				if(state.routes)
					chart.routes(state.routes);
				if(state.stopsData)
					chart.stopsData(state.stopsData);
				if(state.locationData)					
					chart.locationData(state.locationData,true);
		};

		/********* render vehicle location, gets called every 15sec once for each route ***************/
		chart.locationData = function(data,zoom){

				state.locationData = data;

				if(!showVehicles) return;
					
				vehicleLocationMap = d3.map(JSON.parse(JSON.stringify(data)),function(d){ return d.vehicleId })._;
				
			var routeVehicles = g_
								.select('.location')
								.selectAll('.routeVehicles')
								.data(Object.keys(data));

				routeVehicles.enter()
							.append('g')
							.attr('class','routeVehicles');

				routeVehicles.exit().remove();

				routeVehicles.each(function(d){

					var vechicles = d3.select(this).selectAll(".vehicle")
							    .data(data[d],function(d){return d.vehicleId});

						vechicles
							.enter()
							.append("g")
							.attr('class','vehicle')
							.attr('transform',function(d){
									var mappedLatlon = projection([d.lon,d.lat]);

								return 'translate('+(mappedLatlon[0]-(v_width/2))+','+(mappedLatlon[1]-(v_height/2))+')';
							});

						vechicles.exit().transition().duration(1000).remove();

						vechicles
							.transition()
							.duration(zoom?0:3000)
							.attr('transform',function(d){
								var mappedLatlon = projection([d.lon,d.lat]);

								return 'translate('+(mappedLatlon[0]-(v_width/2))+','+(mappedLatlon[1]-(v_height/2))+')';
							})
							.each(function(d){
								var img =	d3.select(this)
												.selectAll('image')
												.data([d]);
									img.enter()
										.append('svg:image')
										.attr('x',0)
										.attr('y',0)
					                    .attr('height',v_height)
					                    .attr('width',v_width)
					                    .attr('xlink:href',function(d){
		                                    return 'images/newBusIcon_.png';
					                    })
					                    .on('click',function(d){
					                    	/**************Capture Click position for Click+Zoom***************************************/
											var translate = d3.transform(d3.select(this.parentNode).attr('transform')).translate;
												capturedLatLon = projection.invert(translate);
										});

									img.exit().remove();

									img
										.attr('transform',function(d){

											var angle = (d.heading < 0)?0:(parseInt(d.heading)+105)%360;
											var ind = (angle < -45)||(angle > 150 && angle < 250)?true:false;
											
											return 'rotate('+(angle)+' '+(v_width/2)+' '+(ind?0:v_height/2)+') scale(1,'+(ind?-1:1)+')';
										});

								var mark =	d3.select(this)
												.selectAll('.mark')
												.data([d]);
									mark.enter()
										.append('rect')
										.attr('width',5)
										.attr('height',5)
										.attr('class','mark')

									mark.exit().remove();

									mark.attr('x',function(d){
											return (v_width/2)-(5/2)
										})
										.attr('y',function(d){
											return (v_height/2)-(5/2)
										})
										.attr('transform',function(d){

											var angle = (d.heading < 0)?0:(parseInt(d.heading)+105)%360;
											var ind = (angle < -45)||(angle > 150 && angle < 250)?true:false;

											return 'rotate('+(angle)+' '+(v_width/2)+' '+(ind?0:v_height/2)+') scale(1,'+(ind?-1:1)+')';
										})
										.style('fill',function(d){
											return (d.color);
										});

							});

				})
				
		};

		/********* render stops ***************/
		chart.stopsData = function(data){

				state.stopsData =data;
			var stops = g_
							.select('.stops')
							.selectAll('.stop')
							.data(Object.keys(data),function(d){return d;});

				stops.enter()
					.append('g')
					.attr('class','stop');

				stops.exit().remove();

				stops.each(function(d) {

					var _ = d3.select(this);

					var stop = _.selectAll("circle")
							    .data(data[d].stops.map(function(_){ 
							    	_.color = data[d].stops.color;
							    	return _;
							    }),function(d){ return d.stopId;});

						stop
							.enter()
							.append("circle")
						    .attr("r", 3)
						    .style('fill','white')
						    .style('stroke',data[d].stops.color)
							.style('stroke-width','1px')
							.on('mouseover',chart.showStoptooltip)
							.on('mouseout',chart.hideStoptooltip);

						stop.exit().attr('r',0).remove();

						stop
							.attr('cx',function(d){ return projection([d.lon,d.lat])[0] })
						    .attr('cy',function(d){ return projection([d.lon,d.lat])[1] })
						    .style('stroke',data[d].stops.color);
					
				});					  	
				
		};

		/********* render routes ***************/
		chart.routes = function(data){
				
				state.routes =data;

				if(!showStops) return;

			var routes = g_
								.select('.routes')
								.selectAll('.route')
								.data(Object.keys(data));

				routes.enter()
						.append('g')
						.attr('class','route');

				routes.exit().remove();

				routes.each(function(d){
					var _ = d3.select(this);

					var paths = _.selectAll("path")
						  		.data(data[d].paths);

						paths
							.enter()
							.append("path")
						    .style('stroke',data[d].paths.color)
							.style('stroke-width','1.5px')
							.style('fill','none');

						paths.exit().attr('r',0).remove();

						paths
							.attr("d", function(d){
						    	var str = "M"
						    		d.path.forEach(function(_,i){
						    			var _i = projection([_.lon,_.lat])
						    			if(i==0)
						    				str = str+_i[0]+','+_i[1];
						    			else
						    				str = str+' L'+_i[0]+','+_i[1];
						    		});
						    		str = str;

						    	return str;
						    })
						    .style('stroke',data[d].paths.color);

				});	
				
		};

		/********* tooltip for stops ***************/
		chart.showStoptooltip = function(d){
			d3.select(this).style('fill',d.color);
			d3.select(this.parentNode)
									.append('rect')
									.attr('width',d.title.length*7)
									.attr('height',30)
									.attr('x',function(){
										return projection([d.lon,d.lat])[0]-(d.title.length*7)/2
									})
									.attr('y',projection([d.lon,d.lat])[1]-40)
									.attr('rx','10')
									.attr('ry','10')
									.style('fill','black')
									.style('stroke','grey')
									.style('stroke-width',1);
								d3.select(this.parentNode).append('text')
									.attr('x',function(){
										return projection([d.lon,d.lat])[0]-(d.title.length*7)/2
									})
									.attr('y',projection([d.lon,d.lat])[1]-20)
									.style('fill','white')
									.text(d.title);
		};
		chart.hideStoptooltip = function(d){
			d3.select(this).style('fill','white');
			d3.select(this.parentNode).selectAll('rect').remove();
			d3.select(this.parentNode).selectAll('text').remove();
		}

		return chart;

	}
		
})(window)