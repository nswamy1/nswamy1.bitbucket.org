"use strict";
angular.module('config', [])
.constant('ENV', {
        'BASE_map_URL1' : 'data/sfmaps/streets.json',
        'BASE_map_URL2' : 'data/sfmaps/arteries.json',
        'BASE_map_neighbour_URL' : 'data/sfmaps/neighborhoods.json',
        'BASE_MAP_GPS_URL':"http://webservices.nextbus.com/service/publicXMLFeed?command=",
        'agency_list_url':"http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=sf-muni"
});