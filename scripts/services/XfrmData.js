'use strict';

myApp.factory("XfrmDataService",function($http,$q){
    var routeColorScale = d3.scale.category10();
    return {

        agencyData:function(data){
            data = $.parseXML(data);
            data = [].map.call(data.querySelectorAll("route"), function(route) {        
                    return {
                      tag: route.getAttribute("tag"),
                      title: route.getAttribute("title")
                    };
                  });

            return data;

        },
        routedata : function(data){

            data = $.parseXML(data);
                var obj = {};
                var route = data.querySelector("route")?data.querySelector("route").getAttribute('tag'):'';

                    obj.stops = [].map.call(data.querySelectorAll("stop"), function(stop) {     
                            return {
                              route:route,
                              stopTag: stop.getAttribute("tag"),
                              stopId:stop.getAttribute("stopId"),
                              title:stop.getAttribute("title"),
                              lat: stop.getAttribute("lat"),
                              lon: stop.getAttribute("lon")
                            };
                          });

                    obj.paths = [].map.call(data.querySelectorAll("path"), function(path) { 

                                var points = [].map.call(path.querySelectorAll("point"), function(point) { 
                                                    return {
                                                              lat: point.getAttribute("lat"),
                                                              lon: point.getAttribute("lon")
                                                            };
                                                });
                            return  {
                                      path: points
                                    };
                          });
            return {route:route,values: obj};
            
        },
        locationData : function(data){
            data = $.parseXML(data);
            
            var route = data.querySelector("vehicle")?data.querySelector("vehicle").getAttribute('routeTag'):'';
            data = [].map.call(data.querySelectorAll("vehicle"), function(vehicle) {        
                    return {
                      vehicleId: vehicle.getAttribute("id"),
                      lat: vehicle.getAttribute("lat"),
                      lon: vehicle.getAttribute("lon"),
                      routeTag:vehicle.getAttribute("routeTag"),
                      heading:vehicle.getAttribute("heading")
                    };
                  });

            return {route:route,
                    values:data,
                    };
            
        }


        
    };
});