'use strict';

var geoMap = angular.module('SFMap-directive',[]);


geoMap.directive('sfmap', function ($http,apiService) {

                return {
                                
                                scope:{                                                                
                                        data: '=',
                                        basemapdata :'=',
                                        id:'@',
                                        api: '=',
                                        locationdata:'=locationdata',
                                        routedata:"=routedata",
                                        config:"=config"
                                },
                                restrict: 'EA',
                                replace: true,
                                link:function(scope, element, attrs){
                                                /********* used to expose API to the outside world(controller) **********************/
                                                scope.api = scope.api || {};

                                                if(scope.id == null || scope.id == undefined)
                                                        return;
                                                /********* Create chart instance once **********************/
                                                if(!scope.chartObject)
                                                        scope.chartObject = d3.SFMap()
                                                                                .containerId(scope.id);

                                                /********* Expose Zoom API to controller **********************/
                                                scope.api.zoom = scope.chartObject.zoom;

                                                /********* Create SVG once **********************/
                                                if(!scope.Chart_main)
                                                        scope.Chart_main = d3.select('#'+scope.id)
                                                                                .append('svg')
                                                                                .call(scope.chartObject);

                                                /********* watch on basemap JeoJSON data **********************/
                                                scope.$watch('basemapdata',function(){
                                                                if(scope.basemapdata === undefined)
                                                                        return;
                                                                scope.chartObject.renderBaseMap(scope.basemapdata);
                                                        
                                                },true);

                                                /********* watch on vehicle location **********************/

                                                scope.$watch('locationdata',function(){
                                                        if(scope.locationdata == undefined)
                                                                return;
                                                        scope.chartObject.locationData(scope.locationdata);
                                                },true);

                                                /********* watch on route data **********************/

                                                scope.$watch('routedata',function(){
                                                                
                                                        if(scope.routedata == undefined )
                                                                return;
                                                        scope.chartObject.routes(scope.routedata);
                                                        scope.chartObject.stopsData(scope.routedata);

                                                },true);

                                                /********* watch on config controls **********************/

                                                scope.$watch('config',function(){
                                                        scope.chartObject.showStops(scope.config.showStops);
                                                        scope.chartObject.showVehicles(scope.config.showVehicles);
                                                },true)

                                }

                }



})
